﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Btn_ArregloInterfaz_Click(object sender, EventArgs e)
        {
            Arreglo_Interfaz arreint = new Arreglo_Interfaz();
            arreint.Show();
            
            
        }

        private void Btn_NumeroInterfaz_Click(object sender, EventArgs e)
        {
            NumeroInterfaz numint = new NumeroInterfaz();
            numint.Show();
        }

        private void Btn_TextoInterfaz_Click(object sender, EventArgs e)
        {
            TextoInterfazcs textointerfaz = new TextoInterfazcs();
            textointerfaz.Show();
        }
    }
}
