﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_ArregloInterfaz = new System.Windows.Forms.Button();
            this.Btn_NumeroInterfaz = new System.Windows.Forms.Button();
            this.Btn_TextoInterfaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Btn_ArregloInterfaz
            // 
            this.Btn_ArregloInterfaz.Location = new System.Drawing.Point(70, 44);
            this.Btn_ArregloInterfaz.Name = "Btn_ArregloInterfaz";
            this.Btn_ArregloInterfaz.Size = new System.Drawing.Size(135, 32);
            this.Btn_ArregloInterfaz.TabIndex = 0;
            this.Btn_ArregloInterfaz.Text = "Arreglo Interfaz";
            this.Btn_ArregloInterfaz.UseVisualStyleBackColor = true;
            this.Btn_ArregloInterfaz.Click += new System.EventHandler(this.Btn_ArregloInterfaz_Click);
            // 
            // Btn_NumeroInterfaz
            // 
            this.Btn_NumeroInterfaz.Location = new System.Drawing.Point(70, 114);
            this.Btn_NumeroInterfaz.Name = "Btn_NumeroInterfaz";
            this.Btn_NumeroInterfaz.Size = new System.Drawing.Size(135, 32);
            this.Btn_NumeroInterfaz.TabIndex = 1;
            this.Btn_NumeroInterfaz.Text = "Numero Interfaz";
            this.Btn_NumeroInterfaz.UseVisualStyleBackColor = true;
            this.Btn_NumeroInterfaz.Click += new System.EventHandler(this.Btn_NumeroInterfaz_Click);
            // 
            // Btn_TextoInterfaz
            // 
            this.Btn_TextoInterfaz.Location = new System.Drawing.Point(70, 178);
            this.Btn_TextoInterfaz.Name = "Btn_TextoInterfaz";
            this.Btn_TextoInterfaz.Size = new System.Drawing.Size(135, 33);
            this.Btn_TextoInterfaz.TabIndex = 2;
            this.Btn_TextoInterfaz.Text = "Texto Interfaz";
            this.Btn_TextoInterfaz.UseVisualStyleBackColor = true;
            this.Btn_TextoInterfaz.Click += new System.EventHandler(this.Btn_TextoInterfaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.Btn_TextoInterfaz);
            this.Controls.Add(this.Btn_NumeroInterfaz);
            this.Controls.Add(this.Btn_ArregloInterfaz);
            this.Name = "Form1";
            this.Text = "Principal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Btn_ArregloInterfaz;
        private System.Windows.Forms.Button Btn_NumeroInterfaz;
        private System.Windows.Forms.Button Btn_TextoInterfaz;
    }
}

