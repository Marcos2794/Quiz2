﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class cls_ArregloInterfaz
    {
        //metodo  para ordenar un arreglo 
        public int[] OrdenarArreglo()
        {
            // creo un arreglo de tipo int
            int[] miarreglo;

            //cargo el arreglo con los datos desordenados
            miarreglo = new int[] { 1, 4, 5, 8, 7, 9, 6, 3, 2 };

            // utilizo Array.Sort para ordenar mi arreglo
            Array.Sort(miarreglo);

            //retorno mi arreglo
            return miarreglo;
        }
    }
}