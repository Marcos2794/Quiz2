﻿namespace WindowsFormsApp1
{
    partial class Arreglo_Interfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_ArregloNumerico = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ArregloOrdenar = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_ArregloNumerico
            // 
            this.Btn_ArregloNumerico.Location = new System.Drawing.Point(174, 222);
            this.Btn_ArregloNumerico.Name = "Btn_ArregloNumerico";
            this.Btn_ArregloNumerico.Size = new System.Drawing.Size(147, 31);
            this.Btn_ArregloNumerico.TabIndex = 0;
            this.Btn_ArregloNumerico.Text = "Ordenar Arreglo";
            this.Btn_ArregloNumerico.UseVisualStyleBackColor = true;
            this.Btn_ArregloNumerico.Click += new System.EventHandler(this.Btn_ArregloNumerico_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Arreglo : 145879632";
            // 
            // txt_ArregloOrdenar
            // 
            this.txt_ArregloOrdenar.Location = new System.Drawing.Point(174, 64);
            this.txt_ArregloOrdenar.Multiline = true;
            this.txt_ArregloOrdenar.Name = "txt_ArregloOrdenar";
            this.txt_ArregloOrdenar.Size = new System.Drawing.Size(147, 120);
            this.txt_ArregloOrdenar.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(559, 367);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.txt_ArregloOrdenar);
            this.tabPage2.Controls.Add(this.Btn_ArregloNumerico);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(551, 338);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ordenar Arreglo";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(551, 338);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Arreglo_Interfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 380);
            this.Controls.Add(this.tabControl1);
            this.Name = "Arreglo_Interfaz";
            this.Text = "Arreglo_Interfaz";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Btn_ArregloNumerico;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_ArregloOrdenar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
    }
}