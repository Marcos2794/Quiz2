﻿namespace WindowsFormsApp1
{
    partial class NumeroInterfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Numeronarcicista = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Num = new System.Windows.Forms.TextBox();
            this.txt_Resul = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Numeronarcicista
            // 
            this.btn_Numeronarcicista.Location = new System.Drawing.Point(9, 141);
            this.btn_Numeronarcicista.Name = "btn_Numeronarcicista";
            this.btn_Numeronarcicista.Size = new System.Drawing.Size(150, 37);
            this.btn_Numeronarcicista.TabIndex = 0;
            this.btn_Numeronarcicista.Text = "Numero Narcicista";
            this.btn_Numeronarcicista.UseVisualStyleBackColor = true;
            this.btn_Numeronarcicista.Click += new System.EventHandler(this.btn_Numeronarcicista_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ingrese un Numero";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Resultado";
            // 
            // txt_Num
            // 
            this.txt_Num.Location = new System.Drawing.Point(172, 24);
            this.txt_Num.Name = "txt_Num";
            this.txt_Num.Size = new System.Drawing.Size(100, 22);
            this.txt_Num.TabIndex = 3;
            // 
            // txt_Resul
            // 
            this.txt_Resul.Location = new System.Drawing.Point(172, 71);
            this.txt_Resul.Name = "txt_Resul";
            this.txt_Resul.Size = new System.Drawing.Size(100, 22);
            this.txt_Resul.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(516, 235);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.txt_Resul);
            this.tabPage2.Controls.Add(this.btn_Numeronarcicista);
            this.tabPage2.Controls.Add(this.txt_Num);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(508, 206);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Numero Narcicista";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // NumeroInterfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 249);
            this.Controls.Add(this.tabControl1);
            this.Name = "NumeroInterfaz";
            this.Text = "NumeroInterfaz";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Numeronarcicista;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Num;
        private System.Windows.Forms.TextBox txt_Resul;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}