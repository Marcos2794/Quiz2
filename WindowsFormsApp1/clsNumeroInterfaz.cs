﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class clsNumeroInterfaz
    {
        //metodo para un numero Narcisista
        public string NumeroNarcisista(int numero)
        {

            int resultado = 0;
            int numeroelevado = 0;
            int potencia = 0;
            string cifras;
            string tiponumero;
            int auxiliar = 0;

            //convierto la variable int numero ne strign cifras
            cifras = Convert.ToString(numero);
            //le indico a la variable potencia su valor segun la extencion del numero ingresado
            potencia = cifras.Length;
            // creo un for para recorrer el string convertido
            for (int i = 0; i < cifras.Length; i++)
            {
                auxiliar = Convert.ToInt32(cifras[i]);
                numeroelevado = auxiliar ^ potencia;
                resultado = (numeroelevado + resultado);


            }
            // valido que el resultado sea igual o no al numero ingresado
            if (resultado==numero)
            {
                tiponumero = "Es un numero narcicista";
            }
            else
            {
                tiponumero = "No es un numero narcicista";
            }



            // aqui me retorna la variable tipo string "tipo de numero"
            // pero por un error devuelvo la variable int resultado
            // con 153 me retorna 152 por eso no se cumple la validacion
            return resultado.ToString();
        }
    }
}
