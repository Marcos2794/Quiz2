﻿namespace WindowsFormsApp1
{
    partial class TextoInterfazcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_Iniciarpalindromo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Palabra = new System.Windows.Forms.TextBox();
            this.txt_verificarPalindromo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(649, 354);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txt_verificarPalindromo);
            this.tabPage1.Controls.Add(this.txt_Palabra);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btn_Iniciarpalindromo);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(641, 325);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Palindromo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(192, 71);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_Iniciarpalindromo
            // 
            this.btn_Iniciarpalindromo.Location = new System.Drawing.Point(32, 188);
            this.btn_Iniciarpalindromo.Name = "btn_Iniciarpalindromo";
            this.btn_Iniciarpalindromo.Size = new System.Drawing.Size(75, 23);
            this.btn_Iniciarpalindromo.TabIndex = 0;
            this.btn_Iniciarpalindromo.Text = "Iniciar";
            this.btn_Iniciarpalindromo.UseVisualStyleBackColor = true;
            this.btn_Iniciarpalindromo.Click += new System.EventHandler(this.btn_Iniciarpalindromo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ingrese una Palabra";
            // 
            // txt_Palabra
            // 
            this.txt_Palabra.Location = new System.Drawing.Point(32, 74);
            this.txt_Palabra.Name = "txt_Palabra";
            this.txt_Palabra.Size = new System.Drawing.Size(198, 22);
            this.txt_Palabra.TabIndex = 2;
            // 
            // txt_verificarPalindromo
            // 
            this.txt_verificarPalindromo.Location = new System.Drawing.Point(32, 142);
            this.txt_verificarPalindromo.Name = "txt_verificarPalindromo";
            this.txt_verificarPalindromo.Size = new System.Drawing.Size(198, 22);
            this.txt_verificarPalindromo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Resultado";
            // 
            // TextoInterfazcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 368);
            this.Controls.Add(this.tabControl1);
            this.Name = "TextoInterfazcs";
            this.Text = "TextoInterfazcs";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_verificarPalindromo;
        private System.Windows.Forms.TextBox txt_Palabra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Iniciarpalindromo;
        private System.Windows.Forms.TabPage tabPage2;
    }
}