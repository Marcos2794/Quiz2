﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class TextoInterfazcs : Form
    {
        public TextoInterfazcs()
        {
            InitializeComponent();
        }

        private void btn_Iniciarpalindromo_Click(object sender, EventArgs e)
        {
            clsTextoInterfaz textoint = new clsTextoInterfaz();
            txt_verificarPalindromo.Text = textoint.palabapalindromo(txt_Palabra.Text);
        }
    }
}
